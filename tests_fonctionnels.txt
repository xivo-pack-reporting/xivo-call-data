Tests fonctionnels
==================

Prérequis
---------

Un XiVO
xivo-call-data installé sur une machine, configurée pour utiliser le XiVO

Tests
-----

* Transfert indirect
A appelle B ; B répond et fait un transfert indirect vers C
Lancer xivo-call-data ==> la ligne associée à l'appel a le flag 'transfered' à 'true'

