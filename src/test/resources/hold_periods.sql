DROP TABLE IF EXISTS hold_periods CASCADE;

CREATE TABLE hold_periods (
    id SERIAL PRIMARY KEY,
    linkedid VARCHAR(150),
    start TIMESTAMP WITHOUT TIME ZONE,
    "end" TIMESTAMP WITHOUT TIME ZONE
);
CREATE INDEX hold_periods__idx__linkedid ON hold_periods(linkedid);