SET ROLE asterisk;

DROP TABLE IF EXISTS attached_data;
CREATE TABLE attached_data (
    id SERIAL PRIMARY KEY,
    id_call_data INTEGER REFERENCES call_data(id),
    key VARCHAR(128),
    value VARCHAR(256)
);
