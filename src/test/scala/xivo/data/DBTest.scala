package xivo.data

import java.text.SimpleDateFormat

import org.scalatest.{BeforeAndAfterEach, BeforeAndAfterAll, FlatSpec, Matchers}
import xivo.data.dbunit.DBUtil

abstract class DBTest(tables: List[String]) extends FlatSpec with Matchers with BeforeAndAfterAll with BeforeAndAfterEach {

  implicit val connection = DBUtil.conn
  val format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

  override def beforeAll() = {
    DBUtil.setupDB("calldata.xml")
  }

  override def beforeEach(): Unit = {
    tables.foreach(DBUtil.cleanTable(_))
  }
}
