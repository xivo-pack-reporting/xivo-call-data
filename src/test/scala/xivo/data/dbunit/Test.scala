package xivo.data.dbunit

import org.scalatest.Matchers
import org.scalatest.FlatSpec

class Test extends FlatSpec with Matchers {
  
  "DBUnit" should "create the tables" in {
    pending
    DBUtil.setupDB("calldata.xml")
  }
  
  it should "insert from CSV" in {
    pending
    DBUtil.insertFromCsv()
  }
  
  it should "create cel CSV" in {
    pending
    DBUtil.createCsv("cel")
  }
}