package xivo.data.dbunit

import java.io.File

import anorm.SQL
import org.dbunit.database.DatabaseConnection
import org.dbunit.dataset.DefaultDataSet
import org.dbunit.dataset.csv.{CsvDataSet, CsvDataSetWriter}
import org.dbunit.dataset.xml.FlatXmlDataSet
import org.dbunit.operation.DatabaseOperation
import xivo.data.ConnectionFactory

object DBUtil {
  
  val HOST = "localhost"
  val DB_NAME = "asterisktest"
  val USER = "asterisk"
  val PASSWORD = "asterisk"
  
  val factory = new ConnectionFactory(HOST, DB_NAME, USER, PASSWORD)
  implicit val conn = factory.getConnection()
  private val dbunitConnection: DatabaseConnection = new DatabaseConnection(conn)

  def setupDB(filename: String) = {
    val dataset = new FlatXmlDataSet(getClass().getClassLoader().getResourceAsStream(filename))
    for (table <- dataset.getTableNames()) {
      val createTable = scala.io.Source.fromInputStream(getClass().getClassLoader().getResourceAsStream(s"${table}.sql")).mkString
      SQL(createTable).execute
    }
    DatabaseOperation.CLEAN_INSERT.execute(dbunitConnection, dataset)
  }

  def createCsv(tableName: String) = {
    var table = dbunitConnection.createTable(tableName);
    var dataset = new DefaultDataSet(table)
    CsvDataSetWriter.write(dataset, new File(getClass().getClassLoader().getResource("CSV/").getFile()))
  }

  def insertFromCsv() = {
    val dataset = new CsvDataSet(new File(getClass().getClassLoader().getResource("CSV/").getFile()))
    DatabaseOperation.CLEAN_INSERT.execute(dbunitConnection, dataset)
  }
  
  def cleanTable(tableName: String) {
    val st = conn.createStatement();
    st.executeUpdate(s"TRUNCATE TABLE $tableName CASCADE")
  }

}