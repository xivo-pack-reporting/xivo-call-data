package xivo.data.integration

import java.text.SimpleDateFormat

import org.joda.time.DateTime
import xivo.data._
import xivo.data.dbunit.DBUtil
import xivo.data.extraction.{CallDataExtractor, CelInterpretor}

class TestCallDataExtractor extends DBTest(List("call_data", "cel", "agent_position")) {

  var interpretor = new CelInterpretor()
  var extractor = new CallDataExtractor(interpretor)

  override def beforeEach() {
    super.beforeEach()
    DBUtil.insertFromCsv()
  }

  "A CallDataExtractor" should "retrieve uniqueid, src_num, src_num, dst_num and call_direction for each call" in {
    val cels = Cel.temporalySorted(0, List())
    AgentPosition.insert(AgentPosition("1000", "2027", None, new DateTime(format.parse("2014-01-01 08:00:00")), None))
    AgentPosition.insert(AgentPosition("1001", "2015", Some("52015"), new DateTime(format.parse("2014-01-01 08:00:00")), None))
    AgentPosition.insert(AgentPosition("5000", "1000", None, new DateTime(format.parse("2015-05-05 08:00:00")), None))
    val callDatas = extractor.extractCallData(cels)
    callDatas.size shouldEqual 12
    for (callData <- callDatas) {
      callData.uniqueId match {
        case "1392999905.102" => callData.dstNum shouldEqual Some("3059")
          callData.callDirection shouldEqual CallDirection.Internal
          callData.srcNum shouldEqual Some("2027")
          callData.srcAgent shouldEqual Some("1000")
          callData.dstAgent shouldEqual None

        case "1392999921.106" => callData.dstNum shouldEqual Some("3059")
          callData.callDirection shouldEqual CallDirection.Internal
          callData.srcNum shouldEqual Some("2027")
          callData.srcAgent shouldEqual Some("1000")
          callData.dstAgent shouldEqual None

        case "1392999944.110" => callData.dstNum shouldEqual Some("3059")
          callData.callDirection shouldEqual CallDirection.Internal
          callData.srcNum shouldEqual Some("2027")
          callData.srcAgent shouldEqual Some("1000")
          callData.dstAgent shouldEqual None

        case "1392999983.117" => callData.dstNum shouldEqual Some("2015")
          callData.callDirection shouldEqual CallDirection.Internal
          callData.srcNum shouldEqual Some("2027")
          callData.srcAgent shouldEqual Some("1000")
          callData.dstAgent shouldEqual Some("1001")

        case "1393000000.119" => callData.dstNum shouldEqual Some("2015")
          callData.callDirection shouldEqual CallDirection.Internal
          callData.srcNum shouldEqual Some("2027")
          callData.srcAgent shouldEqual Some("1000")
          callData.dstAgent shouldEqual Some("1001")

        case "1393000037.121" => callData.dstNum shouldEqual Some("3059")
          callData.callDirection shouldEqual CallDirection.Internal
          callData.srcNum shouldEqual Some("2027")
          callData.srcAgent shouldEqual Some("1000")
          callData.dstAgent shouldEqual None

        case "1393000060.125" => callData.dstNum shouldEqual Some("3046")
          callData.callDirection shouldEqual CallDirection.Internal
          callData.srcNum shouldEqual Some("2015")
          callData.srcAgent shouldEqual Some("1001")
          callData.dstAgent shouldEqual None

        case "1395391644.0" => callData.dstNum shouldEqual Some("**9844203")
          callData.callDirection shouldEqual CallDirection.Outgoing
          callData.srcNum shouldEqual Some("2015")
          callData.srcAgent shouldEqual Some("1001")
          callData.dstAgent shouldEqual None

        case "1395391699.2" => callData.dstNum shouldEqual Some("52015")
          callData.callDirection shouldEqual CallDirection.Incoming
          callData.srcNum shouldEqual Some("0230210082")
          callData.srcAgent shouldEqual None
          callData.dstAgent shouldEqual Some("1001")

        case "1395412138.6" => callData.dstNum shouldEqual Some("**9844203")
          callData.callDirection shouldEqual CallDirection.Outgoing
          callData.srcNum shouldEqual Some("2015")
          callData.srcAgent shouldEqual Some("1001")
          callData.dstAgent shouldEqual None

        case "1415020101.2" => callData.dstNum shouldEqual Some("399")
          callData.callDirection shouldEqual CallDirection.Internal
          callData.srcNum shouldEqual Some("310")
          callData.dstAgent shouldEqual None
          callData.srcAgent shouldEqual None

        case "1430839595.99" => callData.dstNum shouldEqual Some("0230210082")
          callData.callDirection shouldEqual CallDirection.Outgoing
          callData.srcNum shouldEqual Some("1000")
          callData.dstAgent shouldEqual None
          callData.srcAgent shouldEqual Some("5000")

        case _ => fail("We should not find another call : " + callData.uniqueId)
      }
    }

  }

  it should "retrieve the start time, answer time, end time and answer status" in {
    val cels = Cel.temporalySorted(0, List())
    val callDatas = extractor.extractCallData(cels)
    callDatas.size shouldEqual 12
    for (callData <- callDatas) {
      callData.uniqueId match {
        case "1392999905.102" => callData.startTime shouldEqual Some(format.parse("2014-02-21 17:25:05"))
          callData.answerTime shouldEqual Some(format.parse("2014-02-21 17:25:11"))
          callData.endTime shouldEqual Some(format.parse("2014-02-21 17:25:15"))
          callData.status shouldEqual Some("answer")

        case "1392999921.106" => callData.startTime shouldEqual Some(format.parse("2014-02-21 17:25:21"))
          callData.answerTime shouldEqual None
          callData.endTime shouldEqual Some(format.parse("2014-02-21 17:25:37"))

        case "1392999944.110" => callData.startTime shouldEqual Some(format.parse("2014-02-21 17:25:44"))
          callData.answerTime shouldEqual Some(format.parse("2014-02-21 17:26:07"))
          callData.endTime shouldEqual Some(format.parse("2014-02-21 17:26:13"))
          callData.status shouldEqual Some("answer")

        case "1392999983.117" => callData.startTime shouldEqual Some(format.parse("2014-02-21 17:26:23"))
          callData.answerTime shouldEqual Some(format.parse("2014-02-21 17:26:28"))
          callData.endTime shouldEqual Some(format.parse("2014-02-21 17:26:31"))
          callData.status shouldEqual Some("answer")

        case "1393000000.119" => callData.startTime shouldEqual Some(format.parse("2014-02-21 17:26:40"))
          callData.answerTime should equal(None)
          callData.endTime shouldEqual Some(format.parse("2014-02-21 17:26:49"))

        case "1393000037.121" => callData.startTime shouldEqual Some(format.parse("2014-02-21 17:27:17"))
          callData.answerTime shouldEqual Some(format.parse("2014-02-21 17:27:21"))
          callData.endTime shouldEqual Some(format.parse("2014-02-21 17:28:13"))
          callData.status shouldEqual Some("answer")

        case "1393000060.125" => callData.startTime shouldEqual Some(format.parse("2014-02-21 17:27:40"))
          callData.answerTime should equal(None)
          callData.endTime shouldEqual Some(format.parse("2014-02-21 17:27:52"))

        case "1430839595.99" => callData.startTime shouldEqual Some(format.parse("2015-05-05 17:26:35"))
          callData.answerTime shouldEqual Some(format.parse("2015-05-05 17:26:44"))
          callData.endTime shouldEqual Some(format.parse("2015-05-05 17:26:51"))

          case "1395391644.0" | "1395391699.2" | "1395412138.6" | "1415020101.2" =>
        case _ => fail("We should not find another call : " + callData.uniqueId)
      }
    }
  }

  it should "extract the ring on answer duration" in {
    val cels = Cel.temporalySorted(0, List())
    val callDatas = extractor.extractCallData(cels)
    callDatas.size shouldEqual 12
    for (callData <- callDatas) {
      callData.uniqueId match {
        case "1392999905.102" => callData.ringDurationOnAnswer shouldEqual Some(5)
        case "1392999921.106" => callData.ringDurationOnAnswer shouldEqual None
        case "1392999944.110" => callData.ringDurationOnAnswer shouldEqual Some(2)
        case "1392999983.117" => callData.ringDurationOnAnswer shouldEqual Some(5)
        case "1393000000.119" => callData.ringDurationOnAnswer shouldEqual None
        case "1393000037.121" => callData.ringDurationOnAnswer shouldEqual Some(3)
        case "1393000060.125" => callData.ringDurationOnAnswer shouldEqual None
        case "1395391644.0" | "1395391699.2" | "1395412138.6" | "1415020101.2" | "1430839595.99" =>
        case _ => fail("We should not find another call : " + callData.uniqueId)
      }
    }
  }

  it should "put a flag on a call if it has been transfered" in {
    val cels = Cel.temporalySorted(0, List())
    val callDatas = extractor.extractCallData(cels)
    callDatas.size shouldEqual 12
    for (callData <- callDatas) {
      callData.uniqueId match {
        case "1393000037.121" => callData.transfered shouldBe true
        case "1392999905.102" | "1392999921.106" | "1392999944.110" | "1392999983.117" | "1415020101.2"
          | "1393000000.119" | "1393000060.125" | "1395391644.0" | "1395391699.2" | "1395412138.6" | "1430839595.99"=> callData.transfered shouldBe false
        case _ => fail("We should not find another call : " + callData.uniqueId)
      }
    }
  }

  it should "extract the attached data" in {
    val cels = Cel.temporalySorted(0, List())
    val callDatas = extractor.extractCallData(cels)
    callDatas.size shouldEqual 12
    for (callData <- callDatas) {
      callData.uniqueId match {
        case "1392999905.102" => callData.attachedData.size shouldEqual 1
          callData.attachedData.head.key shouldEqual "test"
          callData.attachedData.head.value shouldEqual "bonjour"

        case "1393000037.121" | "1392999921.106" | "1392999944.110" | "1392999983.117" | "1415020101.2"
          | "1393000000.119" | "1393000060.125" | "1395391644.0" | "1395391699.2" | "1395412138.6" | "1430839595.99"=> callData.attachedData.size shouldEqual 0
        case _ => fail("We should not find another call : " + callData.uniqueId)
      }
    }
  }

  it should "extract the hold periods" in {
    val format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val cels = Cel.temporalySorted(0, List())
    val callDatas = extractor.extractCallData(cels)
    callDatas.size shouldEqual 12
    for (callData <- callDatas) {
      callData.uniqueId match {
        case "1415020101.2" => callData.holdPeriods.size shouldEqual 1
          callData.holdPeriods.head.start shouldEqual format.parse("2014-11-03 14:08:30")
          callData.holdPeriods.head.end shouldEqual Some(format.parse("2014-11-03 14:08:33"))
          callData.holdPeriods.head.linkedId shouldEqual "1415020101.2"

        case "1393000037.121" | "1392999921.106" | "1392999944.110" | "1392999983.117" | "1392999905.102"
             | "1393000000.119" | "1393000060.125" | "1395391644.0" | "1395391699.2" | "1395412138.6" | "1430839595.99"=> callData.holdPeriods.size shouldEqual 0
        case _ => fail("We should not find another call : " + callData.uniqueId)
      }
    }
  }
}
