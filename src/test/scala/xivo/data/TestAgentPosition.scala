package xivo.data

import org.joda.time.DateTime

class TestAgentPosition extends DBTest(List("agent_position")) {

  "The AgentPosition singleton" should "return the agent associated to a given line or sda at a given time" in {
    val closedPosition = AgentPosition("1000", "2000", None, new DateTime(format.parse("2014-01-01 08:00:00")), Some(new DateTime(format.parse("2014-01-01 18:00:00"))))
    val openedPosition = AgentPosition("1100", "2100", Some("52100"), new DateTime(format.parse("2014-01-02 08:00:00")), None)
    List(closedPosition, openedPosition).foreach(AgentPosition.insert(_))

    AgentPosition.agentForNumAndTime("2000", format.parse("2014-01-01 09:00:00")) shouldEqual(Some("1000"))
    AgentPosition.agentForNumAndTime("2100", format.parse("2014-01-02 09:00:00")) shouldEqual(Some("1100"))
    AgentPosition.agentForNumAndTime("52100", format.parse("2014-01-02 09:00:00")) shouldEqual(Some("1100"))
    AgentPosition.agentForNumAndTime("2100", format.parse("2014-01-01 09:00:00")) shouldEqual(None)
    AgentPosition.agentForNumAndTime("3000", format.parse("2014-01-02 09:00:00")) shouldEqual(None)
  }
}
