package xivo.data

import anorm._

class TestCallData extends DBTest(List("call_data")) {

  "The CallData singleton" should "insert CallDatas in the database" in {
    val cd1 = CallData(None, "1234456.789", Some("1000"), CallDirection.Internal, Some(format.parse("2014-05-21 12:14:35")), Some(format.parse("2014-05-21 12:14:40")), Some(format.parse("2014-05-21 12:18:35")), Some("answer"), Some(10), false, Some("1300"), Some(CallDirection.Outgoing), Some("2000"), Some("2001"))
    val cd2 = CallData(None, "1234456.777", Some("1001"), CallDirection.Outgoing, Some(format.parse("2014-05-21 12:14:46")), Some(format.parse("2014-05-21 12:14:50")), Some(format.parse("2014-05-21 12:19:35")), Some("answer"), Some(5), true, Some("1301"), Some(CallDirection.Internal))

    CallData.createAll(List(cd1, cd2))

    val res = SQL("SELECT * FROM call_data").resultSet
    res.next shouldBe (true)
    res.getInt("id") should be > 0
    res.getString("uniqueid") shouldEqual ("1234456.789")
    res.getString("dst_num") shouldEqual ("1000")
    res.getString("call_direction") shouldEqual ("internal")
    format.format(res.getTimestamp("start_time")) shouldEqual ("2014-05-21 12:14:35")
    format.format(res.getTimestamp("answer_time")) shouldEqual ("2014-05-21 12:14:40")
    format.format(res.getTimestamp("end_time")) shouldEqual ("2014-05-21 12:18:35")
    res.getString("status") shouldEqual ("answer")
    res.getInt("ring_duration_on_answer") shouldEqual (10)
    res.getBoolean("transfered") shouldBe (false)
    res.getString("src_num") shouldEqual ("1300")
    res.getString("transfer_direction") shouldEqual ("outgoing")
    res.getString("src_agent") shouldEqual "2000"
    res.getString("dst_agent") shouldEqual "2001"

    res.next shouldBe (true)
    res.getInt("id") should be > 0
    res.getString("uniqueid") shouldEqual ("1234456.777")
    res.getString("dst_num") shouldEqual ("1001")
    res.getString("call_direction") shouldEqual ("outgoing")
    format.format(res.getTimestamp("start_time")) shouldEqual ("2014-05-21 12:14:46")
    format.format(res.getTimestamp("answer_time")) shouldEqual ("2014-05-21 12:14:50")
    format.format(res.getTimestamp("end_time")) shouldEqual ("2014-05-21 12:19:35")
    res.getString("status") shouldEqual ("answer")
    res.getInt("ring_duration_on_answer") shouldEqual (5)
    res.getBoolean("transfered") shouldBe (true)
    res.getString("src_num") shouldEqual ("1301")
    res.getString("transfer_direction") shouldEqual ("internal")
    res.getString("src_agent") shouldEqual null
    res.getString("dst_agent") shouldEqual null

    res.next shouldBe (false)
  }

  it should "not crash Anorm with null columns" in {
    val cd1 = CallData(None, "123456", None, CallDirection.Internal, None, None, None, None, None, false, None, None)

    CallData.createAll(List(cd1))
  }

  it should "insert CallData with their associated AttachedData and HoldPeriods" in {
    val cd1 = CallData(None, "1234456.789", Some("1000"), CallDirection.Internal, Some(format.parse("2014-05-21 12:14:35")), Some(format.parse("2014-05-21 12:14:40")), Some(format.parse("2014-05-21 12:18:35")), Some("answer"), Some(10), false, Some("1300"), Some(CallDirection.Outgoing))
    cd1.addAttachedData(AttachedData("key1", "value1"))
    cd1.addAttachedData(AttachedData("key2", "value2"))
    cd1.addHoldPeriod(HoldPeriod(format.parse("2014-05-21 12:15:00"), Some(format.parse("2014-05-21 12:15:15")), cd1.uniqueId))
    cd1.addHoldPeriod(HoldPeriod(format.parse("2014-05-21 12:16:00"), Some(format.parse("2014-05-21 12:16:15")), cd1.uniqueId))

    CallData.createAll(List(cd1))

    var res = SQL("SELECT cd.id, cd.uniqueid FROM call_data cd").resultSet
    res.next() shouldBe true
    val id = res.getInt("id")
    res.getString("uniqueid") shouldEqual "1234456.789"
    res.next() shouldBe false

    res = SQL("SELECT id_call_data, key, value FROM attached_data ORDER BY key ASC").resultSet()
    res.next() shouldBe true
    res.getInt("id_call_data") shouldEqual id
    res.getString("key") shouldEqual "key1"
    res.getString("value") shouldEqual "value1"
    res.next() shouldBe true
    res.getInt("id_call_data") shouldEqual id
    res.getString("key") shouldEqual "key2"
    res.getString("value") shouldEqual "value2"
    res.next() shouldBe false

    res = SQL("""SELECT linkedid, start, "end" FROM hold_periods ORDER BY start ASC""").resultSet()
    res.next() shouldBe true
    res.getString("linkedid") shouldEqual cd1.uniqueId
    res.getString("start") shouldEqual "2014-05-21 12:15:00"
    res.getString("end") shouldEqual "2014-05-21 12:15:15"
    res.next() shouldBe true
    res.getString("linkedid") shouldEqual cd1.uniqueId
    res.getString("start") shouldEqual "2014-05-21 12:16:00"
    res.getString("end") shouldEqual "2014-05-21 12:16:15"
    res.next() shouldBe false
  }

  it should "retrieve callids of pending calls" in {
    val cd1 = CallData(None, "1234456.789", Some("1000"), CallDirection.Internal, Some(format.parse("2014-05-21 12:14:35")), Some(format.parse("2014-05-21 12:14:40")), None, Some("answer"), Some(10), false, Some("1300"), Some(CallDirection.Outgoing))
    val cd2 = CallData(None, "1234456.777", Some("1001"), CallDirection.Outgoing, Some(format.parse("2014-05-21 12:14:46")), Some(format.parse("2014-05-21 12:14:50")), Some(format.parse("2014-05-21 12:19:35")), Some("answer"), Some(5), true, Some("1301"), Some(CallDirection.Internal))
    val cd3 = CallData(None, "1234456.788", Some("1000"), CallDirection.Internal, Some(format.parse("2014-05-21 12:15:35")), Some(format.parse("2014-05-21 12:15:40")), None, Some("answer"), Some(10), false, Some("1300"), Some(CallDirection.Outgoing))
    CallData.createAll(List(cd1, cd2, cd3))

    CallData.getPendingCallIds().sorted shouldEqual List("1234456.789", "1234456.788").sorted
  }

  it should "delete pending calls" in {
    val cd1 = CallData(None, "1234456.789", Some("1000"), CallDirection.Internal, Some(format.parse("2014-05-21 12:14:35")), Some(format.parse("2014-05-21 12:14:40")), None, Some("answer"), Some(10), false, Some("1300"), Some(CallDirection.Outgoing))
    val cd2 = CallData(None, "1234456.777", Some("1001"), CallDirection.Outgoing, Some(format.parse("2014-05-21 12:14:46")), Some(format.parse("2014-05-21 12:14:50")), Some(format.parse("2014-05-21 12:19:35")), Some("answer"), Some(5), true, Some("1301"), Some(CallDirection.Internal))
    val cd3 = CallData(None, "1234456.788", Some("1000"), CallDirection.Internal, Some(format.parse("2014-05-21 12:15:35")), Some(format.parse("2014-05-21 12:15:40")), None, Some("answer"), Some(10), false, Some("1300"), Some(CallDirection.Outgoing))
    cd3.addAttachedData(AttachedData("key", "value"))
    cd3.addHoldPeriod(HoldPeriod(format.parse("2014-05-21 12:15:50"), Some(format.parse("2014-05-21 12:15:50")), cd1.uniqueId))
    CallData.createAll(List(cd1, cd2, cd3))

    CallData.deletePendingCalls()

    val res = SQL("SELECT uniqueid FROM call_data").resultSet
    res.next() shouldBe true
    res.getString("uniqueid") shouldEqual "1234456.777"
    res.next() shouldBe false
  }

  it should "not fail when there is nothing to delete" in {
    CallData.deletePendingCalls()
    CallData.deleteStalePendingCalls()
  }

  it should "delete stale pending calls (older then 1 day)" in {
    val cd0 = CallData(None, "1234456.600", Some("1000"), CallDirection.Internal, Some(format.parse("2014-05-21 11:14:35")), Some(format.parse("2014-05-21 11:14:40")), Some(format.parse("2014-05-21 11:14:50")), Some("answer"), Some(10), false, Some("1300"), Some(CallDirection.Outgoing))
    val cd1 = CallData(None, "1234456.789", Some("1000"), CallDirection.Internal, Some(format.parse("2014-05-21 12:14:35")), Some(format.parse("2014-05-21 12:14:40")), None, Some("answer"), Some(10), false, Some("1300"), Some(CallDirection.Outgoing))
    val cd2 = CallData(None, "1234456.777", Some("1001"), CallDirection.Outgoing, Some(format.parse("2014-05-21 15:14:46")), Some(format.parse("2014-05-21 15:14:50")), None, Some("answer"), Some(5), true, Some("1301"), Some(CallDirection.Internal))
    val cd3 = CallData(None, "1234456.788", Some("1000"), CallDirection.Internal, Some(format.parse("2014-05-22 14:00:35")), Some(format.parse("2014-05-22 14:00:40")), None, Some("answer"), Some(10), false, Some("1300"), Some(CallDirection.Outgoing))
    cd1.addAttachedData(AttachedData("key", "value"))
    cd1.addHoldPeriod(HoldPeriod(format.parse("2014-05-21 12:15:50"), Some(format.parse("2014-05-21 12:15:50")), cd1.uniqueId))
    CallData.createAll(List(cd0, cd1, cd2, cd3))

    CallData.deleteStalePendingCalls()

    val res = SQL("SELECT uniqueid FROM call_data").resultSet
    res.next() shouldBe true
    res.getString("uniqueid") shouldEqual "1234456.600"
    res.next() shouldBe true
    res.getString("uniqueid") shouldEqual "1234456.777"
    res.next() shouldBe true
    res.getString("uniqueid") shouldEqual "1234456.788"
    res.next() shouldBe false
  }

  "The CallData class" should "append attached data to the end of the list" in {
    val cd = EmptyObjectProvider.emptyCallData()
    cd.addAttachedData(AttachedData("key1", "value1"))
    cd.addAttachedData(AttachedData("key2", "value2"))

    cd shouldEqual EmptyObjectProvider.emptyCallData().copy(attachedData=List(AttachedData("key1", "value1"), AttachedData("key2", "value2")))
  }

}
