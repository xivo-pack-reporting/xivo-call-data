package xivo.data

import anorm.SQL
import org.joda.time.DateTime

class TestMain extends DBTest(List("cel", "call_data", "attached_data")) {

  val (cid1, cid2) = ("123546.777", "123456.778")

  "Main" should "generate CallData" in {
    val c1 = EmptyObjectProvider.emptyCel()
    c1.id = 1
    c1.linkedId = cid1
    c1.uniqueId = cid1
    c1.eventType = "CHAN_START"
    c1.eventTime = format.parse("2014-01-01 08:00:00")

    val c2 = EmptyObjectProvider.emptyCel()
    c2.id = 2
    c2.linkedId = cid1
    c2.uniqueId = cid1
    c2.eventType = "LINKEDID_END"
    c2.eventTime = format.parse("2014-01-01 08:30:00")

    val c3 = EmptyObjectProvider.emptyCel()
    c3.id = 3
    c3.linkedId = cid2
    c3.uniqueId = cid2
    c3.eventType = "CHAN_START"
    c3.eventTime = format.parse("2014-01-01 08:16:00")

    val c4 = EmptyObjectProvider.emptyCel()
    c4.id = 4
    c4.linkedId = cid2
    c4.uniqueId = cid2
    c4.eventType = "LINKEDID_END"
    c4.eventTime = format.parse("2014-01-01 08:31:00")

    List(c1, c2, c3, c4).foreach(c => insertCel(c))

    val cd = EmptyObjectProvider.emptyCallData()
    cd.uniqueId = cid1
    CallData.createAll(List(cd))

    Main.generateCallData(2)

    val res = SQL("SELECT uniqueid, end_time FROM call_data").resultSet
    res.next() shouldBe true
    res.getString("uniqueid") shouldEqual cid1
    new DateTime(res.getTimestamp("end_time")) shouldEqual  new DateTime(format.parse("2014-01-01 08:30:00"))
    res.next() shouldBe true
    res.getString("uniqueid") shouldEqual cid2
    new DateTime(res.getTimestamp("end_time")) shouldEqual  new DateTime(format.parse("2014-01-01 08:31:00"))

    res.next() shouldBe false
  }

  private def insertCel(cel: Cel) =
  SQL("""INSERT INTO cel(id, uniqueid, linkedid, eventtype, eventtime, userdeftype, cid_name, cid_num, cid_ani, cid_rdnis, cid_dnid, exten, context, channame, appname, appdata, amaflags, accountcode, peeraccount, userfield, peer)
      VALUES ({id}, {uniqueid}, {linkedid}, {eventtype}, {eventtime}, '', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '')""")
    .on('id -> cel.id, 'uniqueid -> cel.uniqueId, 'linkedid -> cel.linkedId, 'eventtype -> cel.eventType, 'eventtime -> cel.eventTime)
    .executeUpdate

}
