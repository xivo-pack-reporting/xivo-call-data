package xivo.data

import org.joda.time.format.DateTimeFormat
import xivo.data.dbunit.DBUtil

class TestCel extends DBTest(List("cel")) {

  override def beforeEach(): Unit = {
    super.beforeEach()
    DBUtil.insertFromCsv
  }

  "The Cel singleton" should "retrieve the cels ordered by ascending id, starting with the given id, plus them whose linkedid is listed" in {
    val startId = 108018;
    val additionnalCids = List("1392999905.102")

    val res: List[Cel] = Cel.temporalySorted(startId, additionnalCids)

    res.size should be > 0

    var first = res(0).id
    var second = res(1).id
    for (i <- 1 to res.size - 2) {
      first < second shouldBe true
      first = second
      second = res(i + 1).id
    }

    val matchingIds = res.filter(cel => cel.id > startId)
    matchingIds.size should be > 0

    val matchingLinkedids = res.filter(cel => cel.linkedId == "1392999905.102")
    matchingLinkedids.size should be > 0
  }

  it should "not fail if there is no linkedid" in {
    val startId = 108018;
    val additionnalCids = List[String]()

    val res: List[Cel] = Cel.temporalySorted(startId, additionnalCids)

    res.size should be > 0

    val matchingIds = res.filter(cel => cel.id > startId)
    matchingIds.size should be > 0
  }

  it should "retrieve the maximum id" in {
    Cel.getMaxId should equal(Some(114615))
  }

  it should "return None if there is no cel" in {
    DBUtil.cleanTable("cel")
    Cel.getMaxId shouldEqual (None)
  }

  it should "return the eventtime of a cel for a given id" in {
    val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
    val date = format.parseDateTime("2014-02-21 17:26:23").toDate()
    date should equal(Cel.getDateForId(108067))
  }

}