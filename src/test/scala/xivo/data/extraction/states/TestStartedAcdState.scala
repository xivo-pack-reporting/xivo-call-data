package xivo.data.extraction.states

class TestStartedAcdState extends TestState {

  "The StartedAcd state" should "return a Started state on ANWSER on the first channel, and set the srcNum" in {
    cel.eventType = "ANSWER"
    cel.appName = ""
    cel .uniqueId = cel.linkedId + "7"
    cel.cidNum = "200"
    val startedAcd = new StartedAcd(callData, cel)

    startedAcd.processCel(cel).isInstanceOf[Started] shouldBe false
    cel.uniqueId = cel.linkedId
    startedAcd.processCel(cel).isInstanceOf[Started] shouldBe true
    callData.srcNum shouldEqual Some("200")
  }

  it should "return a HangedUp state on LINKEDID_END" in {
    cel.eventType = "LINKEDID_END"
    val startedAcd = new StartedAcd(callData, cel)

    startedAcd.processCel(cel).isInstanceOf[HangedUp] shouldBe true
  }

  it should "reset the dstNum at initialization" in {
    callData.dstNum = Some("3000")
    new StartedAcd(callData, cel)

    callData.dstNum shouldEqual None
  }

}
