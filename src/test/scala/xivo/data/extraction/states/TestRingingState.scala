package xivo.data.extraction.states

import java.text.SimpleDateFormat

class TestRingingState extends TestState {

  val format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S")

  "The Ringing state" should "remember ring start time" in {
    cel.eventTime = format.parse("2014-05-23 14:05:23.148")

    val answered = new Ringing(callData, cel)

    answered.ringStartTime shouldEqual format.parse("2014-05-23 14:05:23.148")
  }
  
  it should "return Answered and set ring duration on ANSWER with application AppDial" in {
    val ringing = new Ringing(callData, cel)
    ringing.ringStartTime = format.parse("2014-05-23 14:05:23.000")
    cel.eventType = "ANSWER"
    cel.appName = "AppDial"
    cel.eventTime = format.parse("2014-05-23 14:05:30.000")
    
    val res = ringing.processCel(cel)
    
    res.isInstanceOf[Answered] shouldBe true
    callData.ringDurationOnAnswer should equal(Some(7))
  }
  
  it should "return Started on HANGUP with AppDial" in {
    cel.eventType = "HANGUP"
    cel.appName = "AppDial"
    var ringing = new Ringing(callData, cel)
      
    ringing.processCel(cel).isInstanceOf[Started] shouldBe true
  }
  
  it should "return HangedUp on LINKEDID_END" in {
    cel.eventType = "LINKEDID_END"
    val ringing = new Ringing(callData, cel)
      
    ringing.processCel(cel).isInstanceOf[HangedUp] shouldBe true
  }

  it should "set the dstNum if it has not been set previously" in {
    cel.eventType = "ANSWER"
    cel.appName = "AppDial"
    cel.cidNum = "1000"
    cel.eventTime = format.parse("2014-05-23 14:05:23.148")
    callData.srcNum = None
    val ringing = new Ringing(callData, cel)

    ringing.processCel(cel)

    callData.dstNum shouldEqual Some("1000")
  }
}