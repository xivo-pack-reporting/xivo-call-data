package xivo.data.extraction.states

import java.text.SimpleDateFormat

import xivo.data.{HoldPeriod, CallDirection}

class TestAnsweredState extends TestState {

  override def beforeEach() {
    super.beforeEach
    cel.chanName = "sip/abcd"
    cel.uniqueId = "123456.789"
    cel.linkedId = "123456.789"
  }

  "The Answered state" should "set the answerTime and the status to 'answer'" in {
    var format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S")
    cel.eventTime = format.parse("2014-05-23 14:05:23.148")

    var answered = new Answered(callData, cel)

    callData.status shouldEqual Some("answer")
    callData.answerTime shouldEqual Some(format.parse("2014-05-23 14:05:23.148"))
  }

  it should "set the 'transfered' flag and the 'transfer_direction' to 'internal' on BLINDTRANSFER" in {
    cel.eventType = "BLINDTRANSFER"
    var answered = new Answered(callData, cel)

    var res = answered.processCel(cel)

    res should be theSameInstanceAs answered
    callData.transfered shouldBe (true)
    callData.transferDirection should equal(Some(CallDirection.Internal))
  }

  it should "set the 'transfered' flag  and the 'transfer_direction' to 'internal' when the channel is ZOMBIE and has the same uniqueid as the original" in {
    cel.eventType = "HANGUP"
    cel.chanName = "sip/abcdefgh-<ZOMBIE>"
    callData.uniqueId = "123456.789"
    cel.uniqueId = callData.uniqueId

    var answered = new Answered(callData, cel)

    var res = answered.processCel(cel)

    res should be theSameInstanceAs answered
    callData.transfered shouldBe (true)
    callData.transferDirection should equal(Some(CallDirection.Internal))
  }

  it should "return HangedUp on LINKEDID_END" in {
    cel.eventType = "LINKEDID_END"
    var answered = new Answered(callData, cel)

    var res = answered.processCel(cel)

    res.isInstanceOf[HangedUp] shouldBe (true)
  }

  it should "set the transfer direction to 'outgoing' on XIVO_OUTCALL if the call is flagged with 'transfered'" in {
    callData.transfered = true
    cel.eventType = "XIVO_OUTCALL"
    var answered = new Answered(callData, cel)

    var res = answered.processCel(cel)

    res should be theSameInstanceAs answered
    callData.transferDirection should equal(Some(CallDirection.Outgoing))
  }

  it should "add a HoldPeriod when receiving a HOLD event" in {
    val format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    cel.eventType = "HOLD"
    cel.eventTime = format.parse("2014-01-01 08:15:15")
    val answered = new Answered(callData, cel)

    val res = answered.processCel(cel)

    res should be theSameInstanceAs answered
    callData.holdPeriods shouldEqual List(HoldPeriod(cel.eventTime, None, cel.linkedId))
  }

  it should "close the last HoldPeriod when receiving a UNHOLD event" in {
    val format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    cel.eventType = "UNHOLD"
    cel.eventTime = format.parse("2014-01-01 08:18:00")
    callData.addHoldPeriod(HoldPeriod(format.parse("2014-01-01 08:15:45"), Some(format.parse("2014-01-01 08:16:00")), cel.linkedId))
    callData.addHoldPeriod(HoldPeriod(format.parse("2014-01-01 08:17:45"), None, cel.linkedId))
    val answered = new Answered(callData, cel)

    val res = answered.processCel(cel)

    res should be theSameInstanceAs answered
    callData.holdPeriods shouldEqual List(HoldPeriod(format.parse("2014-01-01 08:17:45"), Some(cel.eventTime), cel.linkedId),
      HoldPeriod(format.parse("2014-01-01 08:15:45"), Some(format.parse("2014-01-01 08:16:00")), cel.linkedId))
  }
}