package xivo.data.extraction.states

import org.scalatest.BeforeAndAfterEach
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import xivo.data.Cel
import xivo.data.CallData
import xivo.data.extraction.InvalidCelException
import java.text.SimpleDateFormat

class TestOffState extends TestState {

  "The Off state" should "return Started on CHAN_START and set the uniqueid, dstNum, srcNum and startTime" in {
    var format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S")
    cel.eventType = "CHAN_START"
    cel.exten = "1000"
    cel.context = "default"
    cel.linkedId = "123456.789"
    cel.eventTime = format.parse("2014-05-23 14:05:23.148")
    cel.cidNum = "1005"
    var off = new Off(callData)

    var res = off.processCel(cel)

    res.isInstanceOf[Started] shouldBe (true)
    callData.dstNum shouldEqual Some("1000")
    callData.uniqueId shouldEqual "123456.789"
    callData.startTime shouldEqual Some(format.parse("2014-05-23 14:05:23.148"))
    callData.srcNum shouldEqual Some("1005")
  }

  it should "not set the dstNum if the exten equals to 's'" in {
    cel.eventType = "CHAN_START"
    cel.exten = "s"
    var off = new Off(callData)

    var res = off.processCel(cel)

    res.isInstanceOf[Started] shouldBe (true)
    callData.dstNum shouldEqual None
  }

  it should "throw an exception otherwise" in {
    cel.eventType = "ANSWER"
    var off = new Off(callData)
    intercept[InvalidCelException] {
      off.processCel(cel)
    }
  }
}