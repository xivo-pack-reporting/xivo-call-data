package xivo.data.extraction.states

import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}
import xivo.data.{CallData, Cel, EmptyObjectProvider}

class TestState extends FlatSpec with Matchers with BeforeAndAfterEach {

  protected var cel: Cel = null
  protected var callData: CallData = null

  override def beforeEach() = {
    cel = EmptyObjectProvider.emptyCel
    callData = EmptyObjectProvider.emptyCallData
    cel.linkedId = "123456.789"
    callData.uniqueId = "123456.789"
  }

  private class AnonymousState(callData: CallData) extends CelInterpretorState(callData: CallData) {
    override def processCel(cel: Cel) = this
  }

  "Any state" should "parse the ATTACHED_DATA" in {
    var cel = EmptyObjectProvider.emptyCel
    cel.eventType = "ATTACHED_DATA"
    cel.appData = "ATTACHED_DATA,myKey=myValue"

    val state = new AnonymousState(EmptyObjectProvider.emptyCallData)
    state.analyzeCel(cel)

    state.getCallData.attachedData.size shouldEqual 1
    val data = state.getCallData.attachedData(0)
    data.key shouldEqual "myKey"
    data.value shouldEqual "myValue"
  }

  it should "not fail with not properly formatted userfield" in {
    var cel = EmptyObjectProvider.emptyCel
    cel.eventType = "ATTACHED_DATA"
    cel.appData = "ATTACHED_DATA"

    val state = new AnonymousState(EmptyObjectProvider.emptyCallData)
    state.analyzeCel(cel)

    state.getCallData.attachedData.size shouldEqual 0

    cel.appData = "ATTACHED_DATA,myKey:myValue"

    state.analyzeCel(cel)

    state.getCallData.attachedData.size shouldEqual 0
  }
}