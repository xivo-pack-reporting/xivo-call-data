package xivo.data.extraction.states

import xivo.data.CallDirection

class TestStartedState extends TestState {

  "The Started state" should "return an Answered state on ANWSER" in {
    cel.eventType = "ANSWER"
    cel.appName = "AppDial"
    var started = new Started(callData, cel)

    var res = started.processCel(cel)

    res.isInstanceOf[Answered] shouldBe (true)
  }

  it should "return a HangedUp state on LINKEDID_END" in {
    cel.eventType = "LINKEDID_END"
    var started = new Started(callData, cel)

    var res = started.processCel(cel)

    res.isInstanceOf[HangedUp] shouldBe (true)
  }

  it should "set the call direction to 'outgoing' on XIVO_OUTCALL" in {
    cel.eventType = "XIVO_OUTCALL"
    var started = new Started(callData, cel)

    var res = started.processCel(cel)

    res should be theSameInstanceAs started
    callData.callDirection should equal(CallDirection.Outgoing)
  }

  it should "set the call direction to 'incoming' in XIVO_INCALL" in {
    cel.eventType = "XIVO_INCALL"
    var started = new Started(callData, cel)

    var res = started.processCel(cel)

    res should be theSameInstanceAs started
    callData.callDirection should equal(CallDirection.Incoming)
  }

  it should "return a Ringing state on APP_START with appname Dial" in {
    cel.eventType = "APP_START"
    cel.appName = "Dial"
    var started = new Started(callData, cel)

    var res = started.processCel(cel)

    res.isInstanceOf[Ringing] shouldBe (true)
  }

  it should "set the dstNum on ANSWER if it was not set before" in {
    cel.eventType = "ANSWER"
    cel.appName = "Queue"
    cel.cidName = "1200"
    var started = new Started(callData, cel)

    var res = started.processCel(cel)

    callData.dstNum should equal(Some("1200"))
  }

  it should "not set the dstNum on APP_START if it was set before" in {
    cel.eventType = "APP_START"
    cel.appName = "Dial"
    cel.cidAni = "1200"
    callData.dstNum = Some("3000")
    var started = new Started(callData, cel)

    var res = started.processCel(cel)

    callData.dstNum should equal(Some("3000"))
  }

  it should "return a StartedAcd state on OUTCALL_ACD" in {
    cel.eventType = "OUTCALL_ACD"
    val started = new Started(callData, cel)

    started.processCel(cel).isInstanceOf[StartedAcd] shouldBe true
  }
}