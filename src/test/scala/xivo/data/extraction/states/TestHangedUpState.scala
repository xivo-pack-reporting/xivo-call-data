package xivo.data.extraction.states

import java.text.SimpleDateFormat

import xivo.data.HoldPeriod
import xivo.data.extraction.InvalidCelException

class TestHangedUpState extends TestState {

  "The HangedUp state" should "throw an exception when it processes a Cel" in {
    val hangup = new HangedUp(callData, cel)
    intercept[InvalidCelException] {
      hangup.processCel(cel)
    }
  }
  
  it should "set the endTime" in {
    val format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S")
    cel.eventTime = format.parse("2014-05-23 14:05:23.148")
    
    new HangedUp(callData, cel)
    
    callData.endTime shouldEqual(Some(format.parse("2014-05-23 14:05:23.148")))
  }

  it should "close the current hold period" in {
    val format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S")
    callData.addHoldPeriod(HoldPeriod(format.parse("2014-01-01 08:15:45.000"), Some(format.parse("2014-01-01 08:16:00.000")), cel.linkedId))
    callData.addHoldPeriod(HoldPeriod(format.parse("2014-01-01 08:17:45.000"), None, cel.linkedId))
    cel.eventTime = format.parse("2014-01-01 08:18:00.000")

    new HangedUp(callData, cel)

    callData.holdPeriods shouldEqual List(HoldPeriod(format.parse("2014-01-01 08:17:45.000"), Some(cel.eventTime), cel.linkedId),
      HoldPeriod(format.parse("2014-01-01 08:15:45.000"), Some(format.parse("2014-01-01 08:16:00.000")), cel.linkedId))
  }

  it should "not modify an already closed hold period" in {
    val format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S")
    callData.addHoldPeriod(HoldPeriod(format.parse("2014-01-01 08:15:45.000"), Some(format.parse("2014-01-01 08:16:00.000")), cel.linkedId))
    callData.addHoldPeriod(HoldPeriod(format.parse("2014-01-01 08:17:45.000"), Some(format.parse("2014-01-01 08:18:00.000")), cel.linkedId))
    cel.eventTime = format.parse("2014-01-01 08:18:30.000")

    new HangedUp(callData, cel)

    callData.holdPeriods shouldEqual List(HoldPeriod(format.parse("2014-01-01 08:17:45.000"), Some(format.parse("2014-01-01 08:18:00.000")), cel.linkedId),
      HoldPeriod(format.parse("2014-01-01 08:15:45.000"), Some(format.parse("2014-01-01 08:16:00.000")), cel.linkedId))
  }
  
}