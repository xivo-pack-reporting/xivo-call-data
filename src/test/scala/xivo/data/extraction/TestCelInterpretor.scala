package xivo.data.extraction

import org.easymock.EasyMock
import org.easymock.EasyMock._
import org.scalatest.{FlatSpec, Matchers}
import xivo.data.EmptyObjectProvider
import xivo.data.extraction.states.{CelInterpretorState, Off}

class TestCelInterpretor extends FlatSpec with Matchers {
  
  "A CelInterpretor" should "call the State with the Cel and update it" in {
    var interpretor = new CelInterpretor()
    var cel = EmptyObjectProvider.emptyCel
    var state1 = createMock(classOf[CelInterpretorState])
    var state2 = createMock(classOf[CelInterpretorState])
    interpretor.state = state1
    EasyMock.expect(interpretor.state.analyzeCel(cel)).andReturn(state2)
    replay(state1)
    
    interpretor.parseCel(cel)
    verify(state1)
    interpretor.state shouldEqual(state2)
  }
  
  it should "be reset" in {
    var interpretor = new CelInterpretor()
    interpretor.state.isInstanceOf[Off] shouldBe(true)
    
    interpretor.state = createMock(classOf[CelInterpretorState])
    interpretor.reset
    
    interpretor.state.isInstanceOf[Off] shouldBe(true)
  }

  it should "retrieve the CallData from the state" in {
    var interpretor = new CelInterpretor()
    var state = createMock(classOf[CelInterpretorState])
    var cd = EmptyObjectProvider.emptyCallData
    interpretor.state = state
    EasyMock.expect(state.getCallData).andReturn(cd)
    replay(state)
    
    var res = interpretor.getCallData
    
    res should be theSameInstanceAs cd
  }

}