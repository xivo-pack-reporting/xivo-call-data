package xivo.data.extraction

import org.easymock.EasyMock
import org.easymock.EasyMock.createNiceMock
import org.easymock.EasyMock.replay
import org.easymock.EasyMock.verify
import org.scalatest.BeforeAndAfterEach
import org.scalatest.FlatSpec
import org.scalatest.Matchers

import xivo.data.Cel
import xivo.data.EmptyObjectProvider
import xivo.data.dbunit.DBUtil

class TestCallDataExtractor extends FlatSpec with Matchers with BeforeAndAfterEach {
  var interpretor: CelInterpretor = null
  var extractor: CallDataExtractor = null
  var id1 = "123456.789"
  var id2 = "123456.123"
  var c1 = EmptyObjectProvider.emptyCel
  var c2 = EmptyObjectProvider.emptyCel
  var c3 = EmptyObjectProvider.emptyCel
  var c4 = EmptyObjectProvider.emptyCel
  var cd1 = EmptyObjectProvider.emptyCallData
  var cd2 = EmptyObjectProvider.emptyCallData
  var cels = List[Cel](c1, c3, c2, c4)
  implicit val connection = DBUtil.conn

  override def beforeEach() {
    interpretor = createNiceMock(classOf[CelInterpretor])
    extractor = new CallDataExtractor(interpretor)
  }

  "A CallDataExtractor" should "group the cels by linkedid and pass then to the interpretor" in {
    cd1.id = Some(1)
    cd2.id = Some(2)
    c1.linkedId = id1
    c2.linkedId = id1
    c3.linkedId = id2
    c4.linkedId = id2

    interpretor.parseCel(c1)
    interpretor.parseCel(c2)
    EasyMock.expect(interpretor.getCallData()).andReturn(cd1);
    interpretor.reset()
    interpretor.parseCel(c3)
    interpretor.parseCel(c4)
    EasyMock.expect(interpretor.getCallData()).andReturn(cd2);
    interpretor.reset()
    replay(interpretor)

    val res = extractor.extractCallData(cels)

    verify(interpretor)
    res.size should equal(2)
    res(0) should be theSameInstanceAs cd1
    res(1) should be theSameInstanceAs cd2
  }

   it should "return None and reset the interpretor if an exception is thrown" in {
    c1.linkedId = id1
    EasyMock.expect(interpretor.parseCel(c1)).andThrow(new InvalidCelException)
    interpretor.reset()
    replay(interpretor)

    val res = extractor.parseCall(List(c1))

    res shouldBe None
    verify(interpretor)
  }
}