package xivo.data

import anorm._

class TestLastCelId extends DBTest(List("last_cel_id")) {

  "The LastCelId singleton" should "insert or create the last cel id row" in {
    LastCelId.setId(123);
    
    var rs = SQL("SELECT id FROM last_cel_id").resultSet
    rs.next() shouldBe true
    rs.getInt("id") should equal(123)
    rs.next() shouldBe false
    rs.close
    
    LastCelId.setId(456)
    rs = SQL("SELECT id FROM last_cel_id").resultSet
    rs.next() shouldBe true
    rs.getInt("id") should equal(456)
    rs.next() shouldBe false
  }

  it should "retrieve the last cel id row" in {
    SQL("INSERT INTO last_cel_id(id) VALUES (123)").executeUpdate
    
    LastCelId.getId should equal(123)
  }
  
  it should "return 0 if no id is found" in {
    LastCelId.getId should equal(0)
  }

}