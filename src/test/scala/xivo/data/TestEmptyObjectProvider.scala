package xivo.data

import org.scalatest.Matchers
import org.scalatest.FlatSpec
import java.text.SimpleDateFormat
import anorm.SqlParser._
import anorm._
import org.scalatest.BeforeAndAfterEach
import xivo.data.dbunit.DBUtil

class TestEmptyObjectProvider extends FlatSpec with Matchers {
   
  "The EmptyObjectProvider" should "provide a CallData with 'transfered' false" in {
    var callData = EmptyObjectProvider.emptyCallData
    callData.transfered shouldBe(false)
  }
}