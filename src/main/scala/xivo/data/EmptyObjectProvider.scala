package xivo.data

object EmptyObjectProvider {
  def emptyCel(): Cel =
    Cel(0, null, null, null, null, null, null, null, null, null, null,
      null, null, null, 0, null, null, null, null, null, null, None)

  def emptyCallData(): CallData = CallData(None, null, None, CallDirection.Internal, None, None, None, None, None, false, None, None)
}