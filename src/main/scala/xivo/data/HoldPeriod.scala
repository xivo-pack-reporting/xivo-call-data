package xivo.data

import java.util.Date

case class HoldPeriod(var start: Date, var end: Option[Date], linkedId: String)
