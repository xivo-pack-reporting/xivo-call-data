package xivo.data

import java.sql.Connection
import java.util.Date
import anorm.SqlParser._
import anorm._
import org.slf4j.LoggerFactory
import xivo.data.CallDirection.CallDirection

case class CallData(
  var id: Option[Int],
  var uniqueId: String,
  var dstNum: Option[String],
  var callDirection: CallDirection,
  var startTime: Option[Date],
  var answerTime: Option[Date],
  var endTime: Option[Date],
  var status: Option[String],
  var ringDurationOnAnswer: Option[Int],
  var transfered: Boolean,
  var srcNum: Option[String],
  var transferDirection: Option[CallDirection],
  var srcAgent: Option[String] = None,
  var dstAgent: Option[String] = None,
  var attachedData: List[AttachedData] = List(),
  var holdPeriods: List[HoldPeriod] = List()) {

  def addAttachedData(data: AttachedData) = attachedData = attachedData ++ List(data)

  def addHoldPeriod(p: HoldPeriod) = holdPeriods = holdPeriods.+:(p)
}

object CallData {

  val logger = LoggerFactory.getLogger(getClass)

  val callDataInsertStmt = SQL("""INSERT INTO call_data(uniqueid, dst_num, call_direction, start_time, answer_time, end_time, status, ring_duration_on_answer, transfered, src_num, transfer_direction, src_agent, dst_agent)
                VALUES ({uniqueid}, {dst_num}, {call_direction}, {start_time}, {answer_time}, {end_time}, {status}, {ring_duration_on_answer}, {transfered}, {src_num}, {transfer_direction}, {src_agent}, {dst_agent})""")


  val attachedDataInsertStmt = SQL("INSERT INTO attached_data (id_call_data, key, value) VALUES ({id_call_data}, {key}, {value})")
  val holdPeriodsInsertStmt = SQL("""INSERT INTO hold_periods(linkedid, start, "end") VALUES ({linkedid}, {start}, {end})""")

  val callid = get[String]("uniqueid") map {
    case uniqueid => uniqueid
  }

  def deleteStalePendingCalls()(implicit c: Connection) = {
    val rsDate = SQL("SELECT max(start_time) FROM call_data").resultSet()
    if(rsDate.next) {
      val date = rsDate.getTimestamp(1)
      deleteByIds(SQL("SELECT id FROM call_data WHERE end_time IS NULL AND start_time < {maxDate}::timestamp - INTERVAL '1 day'")
        .on('maxDate -> date).as(ids *))
    }
  }

  def getPendingCallIds()(implicit conn: Connection): List[String] = SQL("SELECT uniqueid FROM call_data WHERE end_time IS NULL").as(callid *)

  def callDirectionToString(dir: Option[CallDirection]): Option[String] = {
    dir match {
      case None => None
      case Some(value) => Some(value.toString())
    }
  }

  def createAll(datas: List[CallData])(implicit connection: Connection) = {
    connection.setAutoCommit(false)
    for (data <- datas) {
      try {
        val id = callDataInsertStmt.on(
          'uniqueid -> data.uniqueId, 'dst_num -> data.dstNum, 'call_direction -> data.callDirection.toString, 'start_time -> data.startTime,
          'answer_time -> data.answerTime, 'end_time -> data.endTime, 'status -> data.status, 'ring_duration_on_answer -> data.ringDurationOnAnswer,
          'transfered -> data.transfered, 'src_num -> data.srcNum, 'transfer_direction -> callDirectionToString(data.transferDirection),
          'src_agent -> data.srcAgent, 'dst_agent -> data.dstAgent).executeInsert()
        for (attachedData <- data.attachedData) {
          attachedDataInsertStmt.on('id_call_data -> id, 'key -> attachedData.key, 'value -> attachedData.value).executeInsert()
        }
        for (period <- data.holdPeriods) {
          holdPeriodsInsertStmt.on('linkedid -> period.linkedId, 'start -> period.start, 'end -> period.end).executeInsert()
        }
        connection.commit()
      }  catch {
        case e: Exception => connection.rollback()
          logger.error("Got an exception while creating the call data " + data)
      }
    }
    connection.setAutoCommit(true)
  }

  val ids = get[Int]("id") map { case id => id }

  def deletePendingCalls()(implicit conn: Connection) = {
    deleteByIds(SQL("SELECT id FROM call_data WHERE end_time IS NULL").as(ids *))
  }

  def deleteByIds(ids: List[Int])(implicit c: Connection): Unit = {
    if(ids.size == 0)
      return;
    c.setAutoCommit(false)
    try {
      SQL(s"DELETE FROM attached_data WHERE id_call_data IN (${ids.mkString(",")})").executeUpdate()
      SQL(s"DELETE FROM hold_periods WHERE linkedid IN (SELECT linkedid FROM call_data WHERE id IN (${ids.mkString(",")}))").executeUpdate()
      SQL(s"DELETE FROM call_data WHERE id IN (${ids.mkString(",")})").executeUpdate()
      c.commit()
    } catch {
      case e: Exception => c.rollback()
        throw e
    }
    c.setAutoCommit(true)
  }
}
