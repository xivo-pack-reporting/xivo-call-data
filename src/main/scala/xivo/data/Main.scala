package xivo.data

import java.sql.{Connection, SQLException}
import org.apache.commons.configuration.PropertiesConfiguration
import org.slf4j.LoggerFactory
import xivo.data.extraction.{CallDataExtractor, CelInterpretor}

object Main {

  val ConfigFile = "/etc/xivo-call-data/xivo-call-data.properties"
  val logger = LoggerFactory.getLogger(getClass)

  def main(args: Array[String]): Unit = {
    val config = new PropertiesConfiguration(ConfigFile)
    try {
      implicit val connection = new ConnectionFactory(config.getString("reporting.address"),
        config.getString("reporting.database.name"),
        config.getString("reporting.database.user"),
        config.getString("reporting.database.password")).getConnection()
      logger.info("Starting CallData generation")
      CallData.deleteStalePendingCalls()
      var startId = LastCelId.getId
      var i = 1
      while (Cel.getMaxId != None && startId != Cel.getMaxId.get) {
        logger.info(s"Call data generation : iteration number $i")
        generateCallData(startId)
        startId = LastCelId.getId
        i += 1
      }
    } catch {
      case e: SQLException => logger.error("A SQL exception occurred", e)
      case e: Exception => logger.error("An unexpected exception occurred", e)
    }
  }

  def generateCallData(startId: Int)(implicit conn: Connection) {
    val extractor = new CallDataExtractor(new CelInterpretor)
    val additionnalCids = CallData.getPendingCallIds
    CallData.deletePendingCalls
    logger.info(s"The last CEL id is $startId and the pending calls are $additionnalCids")
    logger.debug("Retrieving the new CEL...")
    val cels = Cel.temporalySorted(startId, additionnalCids)

    logger.debug("Extracting the Call Data from the CEL")
    val callDatas = extractor.extractCallData(cels)
    logger.debug("Inserting the generated Call Data in the database...")
    CallData.createAll(callDatas)

    logger.debug("Storing history information for next generation")
    if (cels.size > 0)
      LastCelId.setId(cels.last.id)
  }

}