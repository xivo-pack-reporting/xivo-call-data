package xivo.data

case class AttachedData (val key: String, val value: String)