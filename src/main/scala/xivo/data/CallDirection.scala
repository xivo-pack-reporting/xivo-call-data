package xivo.data

object CallDirection extends Enumeration {
  type CallDirection = Value
  val Incoming = Value("incoming")
  val Outgoing = Value("outgoing")
  val Internal = Value("internal")
}