package xivo.data

import java.sql.Connection
import java.util.Date
import org.joda.time.format.DateTimeFormat
import anorm._
import anorm.SqlParser._
import scala.collection.mutable.MutableList
import java.util.Calendar

case class Cel(
  var id: Int,
  var eventType: String,
  var eventTime: Date,
  var userDefType: String,
  var cidName: String,
  var cidNum: String,
  var cidAni: String,
  var cidRdnis: String,
  var cidDnid: String,
  var exten: String,
  var context: String,
  var chanName: String,
  var appName: String,
  var appData: String,
  var amaFlags: Int,
  var accountCode: String,
  var peerAccount: String,
  var uniqueId: String,
  var linkedId: String,
  var userField: String,
  var peer: String,
  var callLogId: Option[Int])

object Cel {
  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
  val cal = Calendar.getInstance()

  val simple = get[Int]("id") ~
    get[String]("eventtype") ~
    get[String]("f_eventtime") ~
    get[String]("userdeftype") ~
    get[String]("cid_name") ~
    get[String]("cid_num") ~
    get[String]("cid_ani") ~
    get[String]("cid_rdnis") ~
    get[String]("cid_dnid") ~
    get[String]("exten") ~
    get[String]("context") ~
    get[String]("channame") ~
    get[String]("appname") ~
    get[String]("appdata") ~
    get[Int]("amaflags") ~
    get[String]("accountcode") ~
    get[String]("peeraccount") ~
    get[String]("uniqueid") ~
    get[String]("linkedid") ~
    get[String]("userfield") ~
    get[String]("peer") ~
    get[Option[Int]]("call_log_id") map {
      case id ~ eventtype ~ eventTime ~ userdeftype ~ cidname ~ cidnum ~ cidani ~ cidrdnis ~ cidDnid ~ exten ~
        context ~ channame ~ appname ~ appdata ~ amaflags ~ accountcode ~ peeraccount ~
        uniqueid ~ linkedid ~ userfield ~ peer ~ call_log_id =>
        Cel(id, eventtype, format.parseDateTime(eventTime).toDate(), userdeftype, cidname, cidnum, cidani, cidrdnis, cidDnid, exten,
          context, channame, appname, appdata, amaflags, accountcode, peeraccount,
          uniqueid, linkedid, userfield, peer, call_log_id)
    }

  def temporalySorted(startId: Int, additionnalCids: List[String])(implicit connection: Connection): List[Cel] = {
    var minDate : Date = null
    var request = s"select *, to_char(eventtime, 'YYYY-MM-DD HH24:MI:SS') AS f_eventtime from cel where id > {startId} order by id asc limit 50000"

    if(additionnalCids.size > 0)
      request = s"(($request) UNION ${requestAdditionnalCids(additionnalCids)}) order by id asc"

    val values = getQueryParams(startId, additionnalCids)

    SQL(request).on(values: _*).as(Cel.simple *);
  }

  def getMaxId(implicit connection: Connection):Option[Int] = {
    var rs = SQL("select max(id) from cel").resultSet
    rs.next()
    var result = rs.getInt(1)
    rs.close()
    if(result == 0) None else Some(result)
  }

  def getDateForId(startId: Int)(implicit connection: Connection): Date = {
    var rs = SQL("select to_char(eventtime, 'YYYY-MM-DD HH24:MI:SS') from cel where id = {id}").on('id -> startId).resultSet
    rs.next()
    val result = format.parseDateTime(rs.getString(1)).toDate
    rs.close
    result
  }

  private def getMinDate(startId: Int)(implicit connection: Connection): Date = {
    cal.setTime(getDateForId(startId))
    cal.add(Calendar.DAY_OF_MONTH, -1)
    cal.getTime()
  }

  private def getQueryParams(startId: Int, additionnalCids: List[String]): scala.collection.mutable.MutableList[(String, anorm.ParameterValue[_])] = {
    var values: MutableList[(String, ParameterValue[_])] = MutableList[(String, ParameterValue[_])]("startId" -> startId)
    var i = 1
    for (id <- additionnalCids) {
      values += (s"id$i" -> id)
      i += 1
    }
    values
  }

  private  def requestAdditionnalCids(cids: List[String]): String = {
    var idVars = ""
    for (i <- 1 to cids.size)
      idVars += s"{id$i},"
    if(idVars.size > 0)
      idVars = idVars.substring(0, idVars.size - 1)
    s"select *, to_char(eventtime, 'YYYY-MM-DD HH24:MI:SS') AS f_eventtime from cel where  linkedid in ( $idVars )"
  }
}
