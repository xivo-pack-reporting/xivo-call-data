package xivo.data

import java.sql.Connection
import java.util.Date

import anorm.SqlParser._
import anorm._
import org.joda.time.DateTime

case class AgentPosition(agent: String, lineNumber: String, sda: Option[String], startTime: DateTime, endTime: Option[DateTime])

object AgentPosition {
  val simple = get[String]("line_number") ~
    get[Option[String]]("sda") ~
    get[String]("agent_num") ~
    get[Date]("start_time") ~
    get[Option[Date]]("end_time")  map {
    case line ~ sda ~ agent ~ start ~ end => AgentPosition(agent, line, sda, new DateTime(start), end.map(d => new DateTime(d)))
  }

  def insert(position: AgentPosition)(implicit c: Connection): Unit =
    SQL("INSERT INTO agent_position(line_number, sda, agent_num, start_time, end_time) VALUES ({line}, {sda}, {agent}, {start}, {end})")
      .on('line -> position.lineNumber, 'sda -> position.sda, 'agent -> position.agent, 'start -> position.startTime.toDate, 'end -> position.endTime.map(_.toDate))
      .executeUpdate()

  def agentForNumAndTime(number: String, time: Date)(implicit c: Connection): Option[String] =
    SQL("SELECT agent_num FROM agent_position WHERE start_time <= {time} AND (end_time >= {time} OR end_time IS NULL) AND (line_number = {number}  OR sda = {number})")
    .on('number -> number, 'time -> time).as(get[String]("agent_num") *).headOption
}
