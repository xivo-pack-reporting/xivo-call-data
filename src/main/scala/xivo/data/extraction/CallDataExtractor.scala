package xivo.data.extraction

import java.sql.Connection

import org.slf4j.LoggerFactory
import xivo.data.{AgentPosition, CallData, Cel}

import scala.collection.mutable.{Map, MutableList}

class CallDataExtractor(interpretor: CelInterpretor)(implicit c: Connection) {

  var nonFinishedCids = MutableList[String]()
  val logger = LoggerFactory.getLogger(getClass)

  def extractCallData(cels: List[Cel]): List[CallData] = {
    val groupedCels: Map[String, List[Cel]] = groupCelsByLinkedid(cels)
    var res = MutableList[CallData]()
    for (tuple <- groupedCels) {
      parseCall(tuple._2) match {
        case Some(c) => res += c
        case None =>
      }
    }
    res.toList
  }

  def groupCelsByLinkedid(cels: List[Cel]): Map[String, List[Cel]] = {
    val res = Map[String, List[Cel]]()
    for (cel <- cels) {
      res.get(cel.linkedId) match {
        case None => {
          var list = List[Cel]()
          list ::= cel
          res.put(cel.linkedId, list)
        }
        case Some(list) => res.put(cel.linkedId, list :+ cel)
      }
    }
    res
  }

  def parseCall(cels: List[Cel]): Option[CallData] = {
    for (cel <- cels) {
      try {
        interpretor.parseCel(cel)
      } catch {
        case e: InvalidCelException => {
          logger.warn(s"Invalid cel found: id = ${cel.id}")
          logger.debug("Retrieved information is : " + interpretor.getCallData)
          logger.debug("CELs are : " + cels)
          interpretor.reset
          return None
        }
      }
    }
    val call = interpretor.getCallData
    interpretor.reset()
    if(call.srcNum.isDefined && call.startTime.isDefined)
      call.srcAgent = AgentPosition.agentForNumAndTime(call.srcNum.get, call.startTime.get)
    if(call.dstNum.isDefined && call.startTime.isDefined)
      call.dstAgent = AgentPosition.agentForNumAndTime(call.dstNum.get, call.startTime.get)
    Some(call)
  }

}