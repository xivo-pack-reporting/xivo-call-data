package xivo.data.extraction.states

import xivo.data.Cel
import xivo.data.CallData
import xivo.data.extraction.InvalidCelException

class HangedUp(callData: CallData, cel: Cel) extends CelInterpretorState(callData) {
  callData.endTime = Some(cel.eventTime)
  callData.holdPeriods.headOption.foreach(p => p.end match {
    case None => p.end = Some(cel.eventTime)
    case Some(_) =>
  })
  override def processCel(cel: Cel): CelInterpretorState = {
    throw new InvalidCelException
  }
}