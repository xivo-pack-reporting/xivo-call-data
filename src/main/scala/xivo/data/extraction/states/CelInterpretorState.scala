package xivo.data.extraction.states

import xivo.data.CallData
import xivo.data.Cel
import xivo.data.AttachedData

abstract class CelInterpretorState(callData: CallData) {
  def analyzeCel(cel: Cel): CelInterpretorState = {
    if (cel.eventType equals "ATTACHED_DATA") {
      extractedAttachedData(cel.appData).foreach(attachedData => callData.addAttachedData(attachedData))
      this
    } else {
      processCel(cel)
    }
  }

  private def extractedAttachedData(userField: String): Option[AttachedData] = {
    val splitted = userField.split(",", 2)
    if (splitted.size > 1) {
      val keyValue = splitted(1).split("=")
      if (keyValue.size > 1)
        return Some(AttachedData(keyValue(0), keyValue(1)))
    }
    return None
  }

  def processCel(cel: Cel): CelInterpretorState
  def getCallData: CallData = callData
}