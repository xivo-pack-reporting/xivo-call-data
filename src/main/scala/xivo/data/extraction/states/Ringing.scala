package xivo.data.extraction.states

import xivo.data.Cel
import xivo.data.CallData
import java.util.Date

class Ringing(callData: CallData, cel: Cel) extends CelInterpretorState(callData) {
  var ringStartTime: Date = cel.eventTime
    
  override def processCel(cel: Cel): CelInterpretorState = {
    if(cel.eventType.equals("ANSWER") && cel.appName.equals("AppDial")) {
      val seconds = (cel.eventTime.getTime - ringStartTime.getTime)/1000
      callData.ringDurationOnAnswer = Some(seconds.toInt)
      if(callData.dstNum == None)
        callData.dstNum = Some(cel.cidNum)
      return new Answered(callData, cel)
    } else if(cel.eventType.equals("HANGUP") && cel.appName.equals("AppDial")) {
      return new Started(callData, cel);
    } else if(cel.eventType.equals("LINKEDID_END")) {
      return new HangedUp(callData, cel)
    }
    return this
  }
}