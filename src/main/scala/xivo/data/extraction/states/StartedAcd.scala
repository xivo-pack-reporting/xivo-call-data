package xivo.data.extraction.states

import xivo.data.{CallData, Cel}

class StartedAcd(callData: CallData, cel: Cel) extends CelInterpretorState(callData) {

  callData.dstNum = None

  override def processCel(cel: Cel): CelInterpretorState = {
    if (cel.eventType.equals("ANSWER") && cel.uniqueId.equals(cel.linkedId)) {
      callData.srcNum = Some(cel.cidNum)
      return new Started(callData, cel)
    }
    else if (cel.eventType.equals("LINKEDID_END"))
      return new HangedUp(callData, cel)
    this
  }
}
