package xivo.data.extraction.states

import xivo.data.{CallData, CallDirection, Cel}

class Started(callData: CallData, cel: Cel) extends CelInterpretorState(callData) {

  override def processCel(cel: Cel): CelInterpretorState = {
    if (cel.eventType.equals("ANSWER")) {
      if (callData.dstNum == None)
        callData.dstNum = Some(cel.cidName)
      if(cel.appName.equals("AppDial"))
        return new Answered(callData, cel)
    } else if (cel.eventType.equals("LINKEDID_END")) {
      return new HangedUp(callData, cel)
    } else if (cel.eventType.equals("APP_START") && cel.appName.equals("Dial")) {
      return new Ringing(callData, cel)
    } else if (cel.eventType.equals("XIVO_OUTCALL")) {
      callData.callDirection = CallDirection.Outgoing
    } else if (cel.eventType.equals("XIVO_INCALL")) {
      callData.callDirection = CallDirection.Incoming
    } else if(cel.eventType.equals("OUTCALL_ACD")) {
      return new StartedAcd(callData, cel)
    }
    this
  }
}
