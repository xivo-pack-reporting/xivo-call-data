package xivo.data.extraction.states

import xivo.data.{HoldPeriod, Cel, CallData, CallDirection}

class Answered(callData: CallData, cel: Cel) extends CelInterpretorState(callData) {
  callData.status = Some("answer")
  callData.answerTime = Some(cel.eventTime)
  
  override def processCel(cel: Cel): CelInterpretorState = {
    if(cel.eventType.equals("LINKEDID_END")) {
      return new HangedUp(callData, cel)
    } else if(cel.eventType.equals("HOLD")) {
      callData.addHoldPeriod(HoldPeriod(cel.eventTime, None, cel.linkedId))
    }else if(cel.eventType.equals("UNHOLD")) {
      callData.holdPeriods.headOption.foreach(p => p.end = Some(cel.eventTime))
    } else if(cel.eventType.equals("BLINDTRANSFER")) {
      processTransfer()
    } else if(cel.chanName.contains("ZOMBIE") && cel.uniqueId.equals(callData.uniqueId)) {
      processTransfer()
    } else if(callData.transfered && cel.eventType.equals("XIVO_OUTCALL")) {
      callData.transferDirection = Some(CallDirection.Outgoing)
    }
    return this
  }

  private def processTransfer() {
    callData.transfered = true
    callData.transferDirection = Some(CallDirection.Internal)
  }
}