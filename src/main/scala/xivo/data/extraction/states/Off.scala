package xivo.data.extraction.states

import xivo.data.CallData
import xivo.data.Cel
import xivo.data.extraction.InvalidCelException

class Off(var callData: CallData) extends CelInterpretorState(callData) {

  override def processCel(cel: Cel): CelInterpretorState = {
    if (cel.eventType.equals("CHAN_START")) {
      if (cel.exten != "s")
        callData.dstNum = Some(cel.exten)
      callData.uniqueId = cel.linkedId
      callData.startTime = Some(cel.eventTime)
      callData.srcNum = Some(cel.cidNum)
      return new Started(callData, cel)
    }
    throw new InvalidCelException
  }

}