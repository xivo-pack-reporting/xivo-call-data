package xivo.data.extraction

import xivo.data.{CallData, Cel, EmptyObjectProvider}
import xivo.data.extraction.states.{CelInterpretorState, Off}

class CelInterpretor {

  var state: CelInterpretorState = new Off(EmptyObjectProvider.emptyCallData)

  @throws(classOf[InvalidCelException])
  def parseCel(cel: Cel) = state = state.analyzeCel(cel)

  def getCallData(): CallData = state.getCallData

  def reset() = state = new Off(EmptyObjectProvider.emptyCallData)

}