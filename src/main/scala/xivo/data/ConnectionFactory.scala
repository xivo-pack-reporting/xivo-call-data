package xivo.data

import java.sql.Connection
import java.sql.DriverManager
import java.util.Properties

class ConnectionFactory(host: String, dbName: String, dbUser: String, dbPassword: String) {

  def getConnection(): Connection = {
    Class.forName("org.postgresql.Driver")
    val uri = "jdbc:postgresql://" + host + "/" + dbName
    val props = new Properties();
    props.setProperty("user", dbUser);
    props.setProperty("password", dbPassword);
    props.setProperty("stringtype", "unspecified");
    DriverManager.getConnection(uri, props);
  }

}