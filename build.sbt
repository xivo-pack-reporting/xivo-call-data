import AssemblyKeys._

name := "xivo-call-data"

version := "1.4"

scalaVersion := "2.10.3"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies ++= Seq("org.scalatest" % "scalatest_2.10" % "2.0" % "test",
                            "postgresql" % "postgresql" % "9.1-901.jdbc4",
                            "play" % "anorm_2.10" % "2.1.5",
                            "joda-time" % "joda-time" % "2.0",
                            "org.joda" % "joda-convert" % "1.2",
                            "org.dbunit" % "dbunit" % "2.4.7" % "test",
                            "org.easymock" % "easymock" % "3.1" % "test",
                            "ch.qos.logback" % "logback-classic" % "1.0.7",
                            "commons-configuration" % "commons-configuration" % "1.7")

parallelExecution in Test := false

assemblySettings

mergeStrategy in assembly <<= (mergeStrategy in assembly) {
  (old) => {
    case PathList("org", "apache", "commons", xs@_*) => MergeStrategy.first
    case x => old(x)
  }
}

jarName in assembly := "xivo-call-data.jar"

test in assembly := {}
